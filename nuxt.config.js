module.exports = {
    plugins: ['~plugins/vuetify.js'],
    head: {
        link: [
            {href:'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons',rel:'stylesheet'},
        ]
    },
    css: [
        '@/assets/stylus/main.styl'
    ]
  }