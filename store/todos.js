export const state = () => ({
    list: []
  })
  
  export const mutations = {
    add (state, data) {
      state.list.push({
        description: data.text,
        date: data.date,
        statut: 'TODO',
        created: new Date().toISOString().substr(0, 10),
      });
      state.list.sort( function(a,b){
        var aDate = a.date || a.created;
        var bDate = b.date || b.created;

        if (aDate < bDate)
          return -1;
        if (aDate > bDate)
          return 1;
        if (aDate == bDate)
          return 0;
      })
    },
    remove (state, todo) {
      state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
      todo.statut = "DONE"
    }
  }